package main

type room_t struct {
	// Displayed when entering the room
	Text string `json:"text"`
	Options []option_t `json:"options"`
	ItemsText string `json:"items text"`
	Items []item_t `json:"items"`
	
	X_x string `json:"x-x"`
}

type option_t struct {
//	Id id_t `json:"ID"`
	Text string `json:"text"`
	Dest id_t `json:"dest"`
	EntryText string `json:"entry text"`
	Finish worldFinish `json:"finish"`
	ItemsRequired []struct{
		Item string `json:"item"`
		RemoveItem bool `json:"remove item"` // Whether this item requirement means that the item disappears when you choose the option.
		RemoveReq bool `json:"remove requirement"` // Remove this requirement when used.
		removed bool
		Text string `json:"text"` // Text displayed when the item is used.
		DeniedText string `json:"denied text"`
	} `json:"items required"`
	
	X_x string `json:"x-x"`
}
