package main

import (
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

func (w *world_t)run() worldFinish {
	fmt.Println(w.Text)
	
	var err error
	room := w.Rooms[w.InitialRoom] // Current room
	
	for {
		fmt.Println("---")
		// Print room options:
		for i, option := range room.Options {
			fmt.Printf("%v.\t%s", i + 1, option.Text) ; fmt.Println()
		}
		
		// Print room items:
		if room.ItemsText == "" {
			if len(room.Items) != 0 {
				fmt.Println("Items:")
			}
		} else {
			fmt.Println(room.ItemsText)
		}
		for _, item := range room.Items {
			fmt.Println(item.Name)
			if item.Text != "" {
				fmt.Print("  ")
				fmt.Println(item.Text)
			}
		}
		fmt.Println("---")
		
		// Get & process user choices:
		// Type a number for a room or a name for an item:
		userInputLoop: for {
			// Get user input:
			var inputBytes []byte = make([]byte, 0, 16)
			var inputByte [1]byte
			for {
				var n int
				n, err = os.Stdin.Read(inputByte[:])
				if err != nil || n != 1 {
					if err == io.EOF {
						return worldFinish_quit
					} else {
						fmt.Println("Sorry, what was that?")
						continue userInputLoop
					}
				} else if inputByte[0] == '\r' {
					continue // Ignore it. It could be part of a Windows CRLF sequence.
				}
				
				if inputByte[0] == '\n' { break }
				
				inputBytes = append(inputBytes, inputByte[0])
			}
			input := string(inputBytes)
			/*_, err = fmt.Scan(&input)
			if err != nil {
				if err == io.EOF {
					fmt.Println("Quitting world.")
					return worldFinish_quit
				}
				fmt.Println("Sorry, what was that?")
				continue userInputLoop
			}*/
			
			roomChoice, err := strconv.Atoi(input)
			if err == nil { // Integer?
				if roomChoice <= 0 || roomChoice > len(room.Options) {
					fmt.Println("That's not an option!")
					continue userInputLoop
				}
				
				option := room.Options[roomChoice-1]
				
				// "items required"
				for i, req := range option.ItemsRequired {
					if req.removed { continue }
					
					missingInvItem := true
					for i2 := 0; i2 < len(w.Inventory); i2++ {
						if w.Inventory[i2].Name == req.Item {
							missingInvItem = false
							
							if req.Text != "" {
								fmt.Println(req.Text)
							}
							if req.RemoveItem {
								copy(w.Inventory[i2:len(w.Inventory)-1], w.Inventory[i2+1:]) // TODO: does this work?
								w.Inventory = w.Inventory[:len(w.Inventory)-1]
								i2--
							}
							break
						}
					}
					if missingInvItem {
						if req.DeniedText == "" {
							fmt.Println("You need an item for that.")
						} else {
							fmt.Println(req.DeniedText)
						}
						continue userInputLoop
					}
					
					// "remove requirement"
					if req.RemoveReq {
						// Instead of using req, which is a copy, refer to the original:
						room.Options[roomChoice-1].ItemsRequired[i].removed = true
					}
				}
				
				room = w.Rooms[option.Dest]
				// Print choice's entry text:
				fmt.Println(option.EntryText)
				// End game?
				if option.Finish != worldFinish_null {
					return option.Finish
				}
				if room == nil {
					panic(fmt.Sprintf("“%s” is not a room", option.Dest))
				}
				// Print room text:
				fmt.Println(room.Text)
			} else { // Item?
				input_lc := strings.ToLower(input)
				for i, item := range room.Items {
					if input_lc == strings.ToLower(item.Name) {
						// Add to inventory:
						w.Inventory = append(w.Inventory, item)
						// Remove from room.items:
						copy(room.Items[i:len(room.Items)-1], room.Items[i+1:]) // TODO: does this work?
						room.Items = room.Items[:len(room.Items)-1]
						continue userInputLoop
					}
				}
				// Item not found:
				fmt.Println("That's not an item!")
				continue userInputLoop
			}
			
			break userInputLoop
		}
	}
}
