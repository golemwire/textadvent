package main

import (
	"fmt"
	"os"
)

var (
	world world_t
	
	/*world world_t = world_t{
		name: "TEST A",
		text: "Welcome to TEST A.",
		rooms: map[id_t]room_t{
			"Room 1": {
				
			},
		},
		initialRoom: "Room 1",
	}*/
)

func main() {
	switch len(os.Args) {
	case 1:
		fmt.Println("PROBLEM: no filename supplied!")
		os.Exit(1)
	case 2:
	default:
		fmt.Println("PROBLEM: only supply 1 argument!")
		os.Exit(1)
	}
	
	worldBytes, err := os.ReadFile(os.Args[1])
	if err != nil { panic(err) }
	err = world.decode(worldBytes)
	if err != nil { panic(err) }
	
	switch worldFinish := world.run(); worldFinish {
	case worldFinish_lost:
		fmt.Println("YOU LOSE")
	case worldFinish_won:
		fmt.Println("YOU WIN")
	case worldFinish_neutral:
	case worldFinish_quit:
		fmt.Println("World quitted.")
	default:
		panic(fmt.Sprintf("FATAL: world.run() returned an invalid worldFinish string: %s", worldFinish))
	}
}
