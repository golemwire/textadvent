package main

import (
	"encoding/json"
)

type world_t struct {
//	Name string `json:"name"`
	// The text displayed when run
	Text string `json:"text"`
	
	Inventory []item_t `json:"inventory"`
	
	InitialRoom id_t `json:"initial room"`
	Rooms map[id_t]*room_t `json:"rooms"`
}

type worldFinish string
const (
	worldFinish_null worldFinish = ""
	worldFinish_lost worldFinish = "lose"
	worldFinish_won worldFinish = "win"
	worldFinish_neutral worldFinish = "neutral"
	worldFinish_quit worldFinish = "QUIT" // Internal: for the user quitting the world.
)

func (w *world_t)decode(worldJson []byte) error {
	err := json.Unmarshal(worldJson, w)
	return err
}
