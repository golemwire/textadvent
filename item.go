package main

type id_t string

type item_t struct {
//	Id id_t `json:"ID"`
	// Must not be an integer, like "1" or "-1"
	Name string `json:"name"`
	// Text shown when the item is found
	Text string `json:"text"`
}
